class CheckoutPriceCalculator
    def initialize
    end
    
    def scan(cart = [])
        total_price = 0
        if cart.length > 0
            promo_price = PromoPrice.first
            promo_product = PromoProduct.all
            cart.each do |e|
                # tmp = promo_product.where(products_id: e.products_id).where('amount <= ?', e.amount).first
                tmp = promo_product.select { |f| f.products_id == e.products_id && f.amount <= e.amount }
                if tmp.length > 0
                    e.price = tmp.first.new_price
                end
                total_price += e.price * e.amount
            end
            if total_price > promo_price.total_price
                total_price = total_price - (total_price * promo_price.rate)
            end
            return total_price.round(2)
        else
            0
        end
    end
end