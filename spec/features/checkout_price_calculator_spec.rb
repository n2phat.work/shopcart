require 'rails_helper'

describe CheckoutPriceCalculator do
    it 'should exits' do
        expect{ calc = CheckoutPriceCalculator.new}.not_to raise_error
    end

    it 'test empty cart' do
        cart = []
        calc = CheckoutPriceCalculator.new
        expect(calc.scan(cart)).to eq(0)
    end

    it 'test single item' do
        cart = []
        cart << Cart.new(
            products_id: 1,
            price: Product.find(1).price,
            amount: 1
        )
        calc = CheckoutPriceCalculator.new
        expect(calc.scan(cart)).to eq(Product.find(1).price)
    end

    it 'test 1: products in cart: 001 002 003' do
        cart = []
        cart << Cart.new(
            products_id: 1,
            price: Product.find(1).price,
            amount: 1
        )
        cart << Cart.new(
            products_id: 2,
            price: Product.find(2).price,
            amount: 1
        )
        cart << Cart.new(
            products_id: 3,
            price: Product.find(3).price,
            amount: 1
        )
        calc = CheckoutPriceCalculator.new
        expect(calc.scan(cart)).to eq(66.78)
    end
    it 'test 2: products in cart: 001 001 003' do
        cart = []
        cart << Cart.new(
            products_id: 1,
            price: Product.find(1).price,
            amount: 2
        )
        cart << Cart.new(
            products_id: 3,
            price: Product.find(3).price,
            amount: 1
        )
        calc = CheckoutPriceCalculator.new
        expect(calc.scan(cart)).to eq(36.95)
    end

    it 'test 3: products in cart: 001 001 002 003' do
        cart = []
        cart << Cart.new(
            products_id: 1,
            price: Product.find(1).price,
            amount: 2
        )
        cart << Cart.new(
            products_id: 2,
            price: Product.find(2).price,
            amount: 1
        )
        cart << Cart.new(
            products_id: 3,
            price: Product.find(3).price,
            amount: 1
        )
        calc = CheckoutPriceCalculator.new
        expect(calc.scan(cart)).to eq(73.76)
    end
end