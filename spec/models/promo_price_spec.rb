require 'rails_helper'

describe PromoPrice do
    it 'should exits' do
        expect{ pp = PromoPrice.new }.not_to raise_error
    end

    it 'should have fields: total_price, rate' do
        pp = PromoPrice.new
        expect(pp.total_price).to be_nil
        expect(pp.rate).to be_nil
    end
end