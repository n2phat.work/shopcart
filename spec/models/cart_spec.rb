require 'rails_helper'

describe Cart do
    it 'should exits' do
        expect{ cart = Cart.new }.not_to raise_error
    end

    it 'should have fields: products_id, price, amount' do
        cart = Cart.new
        expect(cart.products_id).to be_nil
        expect(cart.price).to be_nil
        expect(cart.amount).to be_nil
    end

    it 'should belongs_to products' do
        expect(Cart.reflect_on_association(:product).macro).to  eq(:belongs_to)
    end
end