require 'rails_helper'

describe PromoProduct do
    it 'should exits' do
        expect{ pp = PromoProduct.new }.not_to raise_error
    end

    it 'should have fields: products_id, amount, new_price' do
        pp = PromoProduct.new
        expect(pp.products_id).to be_nil
        expect(pp.amount).to be_nil
        expect(pp.new_price).to be_nil
    end
end