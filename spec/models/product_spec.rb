require 'rails_helper'

describe Product do
    it 'should exits' do
        expect{ product = Product.new }.not_to raise_error
    end

    it 'should have fields: code, name, price' do
        product = Product.new
        expect(product.code).to be_nil
        expect(product.name).to be_nil
        expect(product.price).to be_nil
    end

    it 'should has_many carts' do
        expect(Product.reflect_on_association(:carts).macro).to  eq(:has_many)
    end
end