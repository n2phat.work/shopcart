class CreatePromoPrices < ActiveRecord::Migration[5.2]
  def change
    create_table :promo_prices do |t|
      t.float :total_price
      t.float :rate

      t.timestamps
    end
  end
end
