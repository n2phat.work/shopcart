class CreatePromoProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :promo_products do |t|
      t.integer :products_id
      t.integer :amount
      t.float :new_price

      t.timestamps
    end
  end
end
