# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
p1 = Product.find_or_create_by(code: '001')
p1.price = 9.25
p1.name = "Lavender heart"
p1.save!

p2 = Product.find_or_create_by(code: '002')
p2.price = 45.00
p2.name = "Personalised cufflinks"
p2.save!

p3 = Product.find_or_create_by(code: '003')
p3.price = 19.95
p3.name = "Kids T-shirt"
p3.save!

pp = PromoPrice.find_or_create_by(id: 1)
pp.total_price = 60
pp.rate = 0.1
pp.save!

pp = PromoProduct.find_or_create_by(id: 1)
pp.products_id = 1
pp.amount = 2
pp.new_price = 8.50
pp.save!